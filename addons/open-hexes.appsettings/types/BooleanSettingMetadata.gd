extends AbstractSettingMetadata
class_name BooleanSettingMetadata

export var default : bool
const TYPE = 'boolean'

