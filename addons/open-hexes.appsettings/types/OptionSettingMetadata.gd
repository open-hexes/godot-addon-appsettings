extends AbstractSettingMetadata
class_name OptionSettingMetadata


export var default : String
export var options_title : Array
export var options : Array

const TYPE ='select'
