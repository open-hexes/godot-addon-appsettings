extends AbstractSettingMetadata
class_name RangeSettingMetadata

export var default : float
export var minimum : float
export var maximum : float
export var step : float

const TYPE = 'range'
