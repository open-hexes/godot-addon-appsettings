extends Resource
class_name AbstractSettingMetadata

export var category : String
export var section : String
export var slug : String
export var title : String
export var description : String
