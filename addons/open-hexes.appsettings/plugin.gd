tool
extends EditorPlugin


# Helps with your game settings.
# - stores game settings in a JSON file in userland (authoritative source)
# - writes engine settings in a GODOT config file in userland when needed
# - AppSettings singleton can generate a packed scene for CRUD
# - handles migrations


# Shown in the editor's center (main) container,
# when we activate the top AppSettings button.
var main_screen_panel: Control


func _enter_tree() -> void:
	add_autoload_singleton("AppSettings", "res://addons/open-hexes.appsettings/singletons/AppSettings.gd")
	add_main_screen_panel()


func _exit_tree() -> void:
	remove_main_screen_panel()
	remove_autoload_singleton("AppSettings")


# See EditorPlugin.get_plugin_name()
# This creates the top container button (right of AssetLib)
func get_plugin_name() -> String:
	return "AppSettings"


# See EditorPlugin.get_plugin_icon()
# See https://github.com/godotengine/godot/tree/master/editor/icons
func get_plugin_icon():
	return get_editor_interface().get_base_control().get_icon("Save", "EditorIcons")


# See EditorPlugin.make_visible()
func make_visible(visible: bool) -> void:
	if self.main_screen_panel:
		self.main_screen_panel.visible = visible


# See EditorPlugin.has_main_screen()
func has_main_screen() -> bool:
	return true


func add_main_screen_panel() -> void:
	# load instead of preload to avoid missing .import/ issues in fresh clones
	self.main_screen_panel = load("res://addons/open-hexes.appsettings/editor/SettingsEditor.tscn").instance()
	self.main_screen_panel.parent_plugin = self
	get_editor_interface().get_editor_viewport().add_child(self.main_screen_panel)
	make_visible(false)


func remove_main_screen_panel() -> void:
	if self.main_screen_panel:
		if is_instance_valid(self.main_screen_panel):
			self.main_screen_panel.queue_free()
		self.main_screen_panel = null

