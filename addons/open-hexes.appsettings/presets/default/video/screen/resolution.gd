extends AppSettingsSelectInput


func apply_value(raw_value):
	# Convert value stored as array, since we use JSON   6_6
	var value = Vector2(raw_value[0], raw_value[1])
	
	OS.window_size = value
	AppSettings.set_project_setting("display/window/size/width", value.x)
	AppSettings.set_project_setting("display/window/size/height", value.y)
