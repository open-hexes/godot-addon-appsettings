# Godot Addon AppSettings

A singleton and afferent tooling to help handle settings in Godot apps and games.

See [the addon's README](./addons/open-hexes.appsettings/README.md).
